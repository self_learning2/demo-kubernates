package ru.ovechkin.rabbitmq.demo.exception;

public class IdEmptyException extends Exception {

    public IdEmptyException() {
        super("Id is null");
    }

}