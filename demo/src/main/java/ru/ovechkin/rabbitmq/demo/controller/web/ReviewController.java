package ru.ovechkin.rabbitmq.demo.controller.web;

import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import ru.ovechkin.rabbitmq.demo.api.srervice.IReviewService;
import ru.ovechkin.rabbitmq.demo.consts.Constant;
import ru.ovechkin.rabbitmq.demo.dto.ReviewDTO;
import ru.ovechkin.rabbitmq.demo.exception.IdEmptyException;
import ru.ovechkin.rabbitmq.demo.exception.ReviewIsNullException;
import ru.ovechkin.rabbitmq.demo.exception.ReviewNotFoundException;

@Controller
@RequestMapping("/review")
public class ReviewController {

    @Autowired
    private RabbitTemplate template;

    @Value("${spring.rabbitmq.template.exchange}")
    private String topic;

    @Autowired
    private IReviewService reviewService;

    @GetMapping("/main")
    public ModelAndView findAll() {
        return new ModelAndView("main", "reviewsDTO", reviewService.findAll());
    }

    @GetMapping("/edit/form/{id}")
    public ModelAndView getEditForm(
            @PathVariable("id") final String reviewId
    ) throws IdEmptyException, ReviewNotFoundException {
        return new ModelAndView(
                "edit-form",
                "reviewDTO",
                reviewService.findById(reviewId));
    }

    @PostMapping("/edit/post/{id}")
    public String updateReview(
            @PathVariable("id") final String reviewId,
            @ModelAttribute("reviewDTO") final ReviewDTO reviewDTO
    ) throws IdEmptyException, ReviewNotFoundException, ReviewIsNullException {
        reviewService.updateById(reviewId, reviewDTO);
        return Constant.REDIRECT;
    }

    @GetMapping("/remove/{id}")
    public String remove(
            @PathVariable("id") final String reviewId
    ) {
        template.convertAndSend(topic, Constant.DELETE_KEY, reviewId);
        return Constant.REDIRECT;
    }

    @GetMapping("/create/form")
    public ModelAndView getCreateForm() {
        return new ModelAndView(
                "create-form",
                "reviewDTO",
                new ReviewDTO());
    }

    @PostMapping("/create/post")
    public String create(
            @ModelAttribute("reviewDTO") final ReviewDTO reviewDTO
    ) {
        template.convertAndSend(topic, Constant.CREATE_KEY, reviewDTO);
        return Constant.REDIRECT;
    }

}