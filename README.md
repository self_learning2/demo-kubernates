# KUBERNETES PROJECT

Это тестовый проект для того, чтобы познакомиться с k8s, пишу для себя и для знакомых, которым мб это понадобится

## Getting started

Для начала надо установить миникуб (прим. это кластер с одной нодой спецом для тестов).

Пользовался этой [инструкцией](https://minikube.sigs.k8s.io/docs/start/) 

Нужно было следующее:

```shell script
 curl -LO https://storage.googleapis.com/minikube/releases/latest/minikube-linux-amd64
 sudo install minikube-linux-amd64 /usr/local/bin/minikube
```

Миникубик установлен, однако он может быть развёрнут и как виртуальная машина и как контейнер.
Для того, чтобы это определить - надо скормить ему нужный [драйвер](https://minikube.sigs.k8s.io/docs/drivers/ 
"Список поддерживаемых драйверов").

Тут сделано на основе докера по [официальной инструкции](https://docs.docker.com/engine/install/ubuntu/)
```shell script
 sudo apt-get update

 sudo apt-get install \
     apt-transport-https \
     ca-certificates \
     curl \
     gnupg \
     lsb-release
 curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo gpg --dearmor -o /usr/share/keyrings/docker-archive-keyring.gpg
 
 echo \
   "deb [arch=amd64 signed-by=/usr/share/keyrings/docker-archive-keyring.gpg] https://download.docker.com/linux/ubuntu \
   $(lsb_release -cs) stable" | sudo tee /etc/apt/sources.list.d/docker.list > /dev/null
 
 sudo apt-get update
 
 sudo apt-get install docker-ce docker-ce-cli containerd.io
```

Затем надо задать установленный докер в качестве драйвера по умолчанию для запуска миникуба

```shell script
 minikube config set driver docker
```

Запускаем миникубик

```shell script
 minikube start
```

Затем надо установить то, через что можно взаимодействовать с k8s через терминал - ``kubectl``

Тут уже не помню как точно делал, но вроде вот эта [ссылка](https://kubernetes.io/ru/docs/tasks/tools/install-kubectl/) полезная

Потом заходим в папку с .yaml файлами и активируем всё по очереди.
В этих .yaml файлах хранятся настройки с помощью которых создаются pod'ы и сервисы. 
Pod'ы содержат контейнеры с приложениями.
Сервисы определяют доступность этих подов внутри кластера или извне.

_Можно из терминала всё сделать и без .yaml файлов, но так удобнее_
```shell script
 kubectl apply -f  postgres-workload.yaml
 kubectl apply -f  pgadmin-workload.yaml
 kubectl apply -f  rabbitmq-workload.yaml
```

Pgadmin связывает внешку с внутренним контейнером postgres.
Надо зайти в pgadmin по адресу ``ip_адрес_миникубика:порт_пгадмина``.
Логин ``admin@example.com`` пароль ``admin``. Надо подключить к базе в контейнере и создать БД с нужным названием.
У меня это rabbit-review-manager.

Затем надо собрать image своего приложения.

_Пока надо вручную в application.properties самого джарника прописать адреса подов с postgres и rabbitmq._
_Узнать можно с помощью следующей комманды:_

```shell script
 kubectl get all -o wide
```

Указываем, что для сборки image надо использовать тот докер, который использует minikube.

```shell script
 eval $(minikube docker-env)
```

Собираем image из папки с .jar файлом

```shell script
 docker build --no-cache --tag demo-backend:latest .
```

Запускаем .jar в minikube

```shell script
 kubectl apply -f jar-workload.yaml
```

Проверить все сервисы/деплойменты/поды/реплики с ip адресами:

```shell script
 kubectl get all -o wide
```

Проверить как там pod работает с бегающим контейнером можно так:

```shell script
 kubectl logs *название запущенного пода*
```

Запускаем ингресс контроллер для доступа к кластеру

```shell script
 minikube addons enable ingress  
```

Информация о запущенном ингресс должна отодбражаться по команде

```shell script
 kubectl get pods -n kube-system
```

И затем надо применить ресурс для ингресса (без ресурса ингресс контроллер не работает)

```shell script
 kubectl apply -f ingress-resource-workload.yaml
```

Узнаём адрес с которого ингресс перенаправляет трафик по команде

```shell script
 kubectl get ingress
```

Идём в браузер, вбиваем ip, url и готово
